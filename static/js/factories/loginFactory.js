App.factory('loginFactory', loginF);
loginF.$inject = ['$http'];

function loginF($http){
    return {
        login: function(user, psw) {

            const log_token = user + ':' + psw;
            $http.defaults.headers.get = { 'Authorization' : 'Basic ' + window.btoa(unescape(encodeURIComponent( log_token )))};

            return $http.get('http://34.238.190.233/login');
        }
    }
}