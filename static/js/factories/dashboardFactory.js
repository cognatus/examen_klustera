App.factory('dashboardFactory', dashboardF);
dashboardF.$inject = ['$http'];

function dashboardF($http){
    return {
        getKpis: function(branch_id,from_d,to_d,from_hour,to_hour) {
            //in case that some values were undefined, we'll set a default value
            //for this demo, the values will be static in case that doesn't exist, but this has to be different to prod
            branch_id = branch_id || 911; 
            from_d = from_d || 0;
            to_d = to_d || 0;
            from_hour = from_hour || 0;
            to_hour = to_hour || 0;
            return $http.get('http://34.238.190.233/get_kpis/' + 
                                branch_id + '/' + 
                                from_d + '/' + 
                                to_d + '/' + 
                                from_hour + '/' + 
                                to_hour
                            );
        }
    }
}