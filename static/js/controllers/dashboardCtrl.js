'use strict';

App.controller('DashboardCtrl', function ($scope, $rootScope, $http, $location, $cookies, dashboardFactory) {

    var vm = $scope;
    var vmc = $rootScope;
    var fc = dashboardFactory;
    vmc.loader = true;

    vm.branch_id = '';
    vm.from_d = '';
    vm.to_d = '';
    vm.from_hour = '';
    vm.to_hour = '';
    vm.kpis = {};

    vm.initKpis = function () {
        if ($cookies['x-access-token']) {
            $http.defaults.headers.get = { 'x-access-token' : $cookies['x-access-token']};
            var fecha = new Date();
            fecha = fecha.toISOString().split('T');
            vm.branch_id = '911';
            vm.from_d = fecha[0];
            vm.to_d = fecha[0];
            vm.from_hour = '00';
            vm.to_hour = fecha[1].split(':')[0];
            $('[data-toggle="datepicker"]').datepicker({ 
                format: 'yyyy-mm-dd',
                endDate: fecha
            });
            vm.getInfoKpis();
        } else {
            vmc.loader = false;
            alert('Error de autenticacion, inicie sesion');
            $location.path('/');
        }
    }

    vm.getInfoKpis = function () {
        if( new Date(vm.from_d) > new Date(vm.to_d) ) {
            alert('La primer fecha no puede ser despues de la segunda, modifique su peticion');
        } else{
            vmc.loader = true;
            fc.getKpis(vm.branch_id, vm.from_d, vm.to_d, vm.from_hour, vm.to_hour)
                .success(function (data, status, headers, config) {
                    vm.kpis = data.kpis;
                    vmc.loader = false;
                })
                .error(function (err) {
                    vmc.loader = false;
                    if (err.message === 'Token is missing!' || err.message === 'Token Expired!') {
                        alert('Error de autenticacion, inicie sesion');
                    } else {
                        alert('Error obteniendo KPI\'s');
                    }
                    $location.path('/');
                });
        }
    };

});