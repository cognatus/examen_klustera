'use strict';

App.controller('LoginCtrl', function($scope, $rootScope, $http, $location, $cookies, loginFactory) {

    var vm = $scope;
    var vmc = $rootScope;
    var fc = loginFactory;
  
    vm.log = function() {
        vmc.loader = true;
        fc.login(vm.user, vm.psw)
        .success(function(data, status, headers, config) {
            $cookies['x-access-token'] = data.token;
            $location.path('/dashboard');
            vmc.loader = false;
        })
        .error(function(err) {
            $httpProvider.defaults.headers.get = { 'Authorization' : ''};
            alert('Error con inicio de sesion');
        });
    };
  
  });