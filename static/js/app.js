'use strict';

var App = angular.module('App', ['ngRoute','ngCookies']);

App.config(function($routeProvider) {
  $routeProvider.when('/', {
    controller : 'LoginCtrl',
    templateUrl: '/static/partial/login/login.html',
  });
  $routeProvider.when('/dashboard', {
    controller : 'DashboardCtrl',
    templateUrl: '/static/partial/dashboard/index.html',
  });
  $routeProvider.otherwise({
    redirectTo : '/'
  });
});

/*App.controller('MainCtrl', function($scope, $rootScope, $log, $http, $routeParams, $location, $route) {

  $scope.invite = function() {
    $location.path('/invite');
  };

  $scope.update = function(guest) {
    $location.path('/update/' + guest.id);
  };

  $scope.delete = function(guest) {
    $rootScope.status = 'Deleting guest ' + guest.id + '...';
    $http.post('/rest/delete', {'id': guest.id})
    .success(function(data, status, headers, config) {
      for (var i=0; i<$rootScope.guests.length; i++) {
        if ($rootScope.guests[i].id == guest.id) {
          $rootScope.guests.splice(i, 1);
          break;
        }
      }
      $rootScope.status = '';
    });
  };

});*/
