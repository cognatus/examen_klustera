## Crear el entorno virtual y Activacion
```bash
$ mkvirtualenv myvenv
$ WORKON myvenv
```

# Instalación de las dependencias
```bash
$ pip install -r requirements.txt
```

# Ejecutar el proyecto
```bash
$ python manage.py migrate
$ python manage.py runserver
```
